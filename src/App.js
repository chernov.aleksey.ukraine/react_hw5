import "./App.scss";
import Header from "./Components/Header/Header";
import { BrowserRouter } from "react-router-dom";
import AppRoutes from "./Routes";
import { Provider } from "react-redux";
import store from "./store";


const App = () => {
  

  return (
    <Provider store={store}>
      <BrowserRouter>
        <div className="App">
          <Header />
          <AppRoutes />
          
        </div>
      </BrowserRouter>
    </Provider>
  );
};

export default App;
