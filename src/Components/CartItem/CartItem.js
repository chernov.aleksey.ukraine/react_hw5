import React from "react";
import "./CartItem.scss";
import PropTypes from "prop-types";
import { useDispatch } from "react-redux";
import { addToFavoriteAC, chosenToOperateAC, isModalOpenAC, moveFromFavoriteAC } from "../../store/header/actionCreators";


const CartItem = ({
   
  name,
  art,
  color,
  price,
  url,
  isCart,
  isFavorite,
   
 
}) => {
  


  const dispatch = useDispatch();

 


  return (
    <div className="itemcard">
      <div className="itemcardheader">
        <p className="itemart">code: {art}</p>
        <div className="iconcontainer">
          {isFavorite ? (
            <p
              onClick={() => {
             dispatch(moveFromFavoriteAC(art));
                
              }}
            >
              &#9733;
            </p>
          ) : (
            <p
                onClick={() => {
              dispatch(addToFavoriteAC(art));
             
              }}
            >
              &#9734;
            </p>
          )}
          <div className="carticonholder">
            {isCart ? (
              <p
                onClick={() => {
                  dispatch(chosenToOperateAC({ name, art }));
                  dispatch(isModalOpenAC(true));
                   
                }}
              >
                REMOVE
              </p>
            ) : (
              <img
                  onClick={() => {
                }}
                src="./cart.png"
                alt=""
              />
            )}
          </div>
        </div>
      </div>
      <div className="itemphotocontainer">
        <img src={url} alt="img" />
      </div>
      <p className="itemname">{name}</p>
      <p className="itemcolor">color: {color}</p>
      <p className="itemprice">
        {price} <span>гр.</span>
      </p>
    </div>
  );
};

CartItem.propTypes = {
  
  name: PropTypes.string.isRequired,
  art: PropTypes.number.isRequired,
  color: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
  isCart: PropTypes.bool.isRequired,
  isFavorite: PropTypes.bool.isRequired,
  
};
export default CartItem;

