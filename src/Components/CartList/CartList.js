import React from "react";
import CartItem from "../CartItem/CartItem";
import "./CartList.scss"; 
import { useSelector } from "react-redux";

const CartList = () => {
  const array = useSelector((store) => store.header.array);
  return (
    <div className="itemcontainer">
          {array.map(({ name, art, color, price, url, isFavorite, isCart }) => (
               isCart ?  <CartItem
            key={art}
            name={name}
            art={art}
            color={color}
            price={price}
            url={url}
            isFavorite={isFavorite}
            isCart={isCart}
            
          /> : null 
      
      ))}
    </div>
  );
};
CartList.propTypes = {};
export default CartList;
