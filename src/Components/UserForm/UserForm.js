import React from "react";
import { Formik, Form } from "formik";
import * as yup from "yup";
import CustomInput from "../../Components/CustomInput/CustomInput";
import { useDispatch } from "react-redux";
import { onCheckoutAC } from "../../store/header/actionCreators";

function UserForm() {
  const dispatch = useDispatch();

  const handleSubmit = async (values, { resetForm }) => {
    console.log("_____________________________________");
    console.log("Client who has bought the commodities:");
    console.log(values);
    console.log("_____________________________________");
    resetForm();
    dispatch(onCheckoutAC());
    console.log("_____________________________________");
  };

  const initialValues = {
    name: "",
    surname: "",
    age: "",
    deliver: "",
    tel: "",
  };

  const schema = yup.object().shape({
    name: yup
      .string()
      .min(3, "Min 3 characters")
      .max(24, "Max 24 characters")
      .matches(/[a-zA-z\s]/g, "Should contain only characters and space")
      .required(),
    surname: yup
      .string()
      .min(3, "Min 3 characters")
      .max(24, "Max 24 characters")
      .matches(/[a-zA-z\s]/g, "Should contain only characters and space")
      .required(),
    age: yup
      .number()
      .min(18, "Age should be greater than 18")
      .max(81, "Age should be less than 81")
      .required()
      .positive()
      .integer(),
    deliver: yup.string().min(3, "Min 3 characters").required(),
    tel: yup.number().required().positive().integer(),
  });

  return (
    <div className="PurchaseFormContainer">
      <div className="FormContainer">
        <h1>Purchase FORM</h1>
        <Formik
          initialValues={initialValues}
          onSubmit={handleSubmit}
          validationSchema={schema}
        >
          {({ isValid }) => (
            <Form>
              <CustomInput name="name" type="text" placeholder="Name" />
              <CustomInput name="surname" type="text" placeholder="Surname" />
              <CustomInput name="age" type="text" placeholder="Age" />
              <CustomInput
                name="deliver"
                type="text"
                placeholder="Address of the delivery"
              />
              <CustomInput name="tel" type="text" placeholder="Telephone" />

              <button type="submit" disabled={!isValid}>
                CHECKOUT
              </button>
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
}

export default UserForm;
