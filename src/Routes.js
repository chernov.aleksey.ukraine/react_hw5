import React from "react";
import { Route, Routes } from "react-router-dom";
import Home  from "./pages/Home/Home";
import Cart from "./pages/Cart/Cart";
import Favorite from "./pages/Favorite/Favorite";
 

const  AppRoutes = () => (
  <Routes>
    <Route path="/" element={<Home />} />
    <Route path="/cart" element={<Cart />} />
    <Route path="/favorite" element={<Favorite />} />
  </Routes>
);

AppRoutes.propTypes = {};
AppRoutes.defaultProps = {};

export default  AppRoutes;
