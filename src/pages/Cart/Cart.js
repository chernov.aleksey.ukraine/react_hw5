import React from "react";
import CartList from "../../Components/CartList/CartList";
import UserForm from "../../Components/UserForm/UserForm";
import Modalwindow from "../../Components/Modalwindow/Modalwindow";
import './Cart.scss'



const Cart = () => {
  

  return (
    <div className="CartContainer">
      <CartList />
      <UserForm/>
      <Modalwindow
        operation={"move"}
        headertext={"Removing item to the cart."}
        maintext1={"Do you want to remove "}
        maintext2={" from the cart?"}
      />
    </div>
  );
};

export default Cart;
