import React from "react";
import FavoriteList from "../../Components/FavoriteList/FavoriteList";
import Modalwindow from "../../Components/Modalwindow/Modalwindow";

const Favorite = () => {
  return(
 <div>
   <FavoriteList />
   <Modalwindow
     operation={"add"}
     headertext={"Adding item to the cart."}
     maintext1={"Do you want to add"}
     maintext2={"to the cart?"}
   />
 </div>)
};

export default Favorite;
