import { combineReducers } from "redux";
import headerReducer from "./header/reducer";

const appReducer = combineReducers({
  header: headerReducer,

});

export default appReducer;
