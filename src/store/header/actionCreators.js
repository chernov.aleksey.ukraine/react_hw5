import {
  GET_ARRAY,
  IS_MODAL_OPEN,
  ADD_TO_FAVORITE,
  MOVE_FROM_FAVORITE,
  CHOSEN_TO_OPERATE,
  ADD_TO_CART,
  MOVE_FROM_CART,
  ON_CHECKOUT,
} from "./actions";
 
 
export const getArrayAC = () => async (dispatch) => {
    let array = []

    if (localStorage.getItem("array")) {
      array = JSON.parse(localStorage.getItem("array"))
    } else {   array = await fetch('./items.json').then(res => res.json()) }
   
    // array = await fetch("./items.json").then((res) => res.json());
    
 dispatch({ type: GET_ARRAY, payload: array });

};

export const isModalOpenAC = (payload) => ({ type: IS_MODAL_OPEN, payload })
export const addToFavoriteAC = (payload) => ({ type: ADD_TO_FAVORITE, payload});
export const moveFromFavoriteAC = (payload) => ({ type: MOVE_FROM_FAVORITE, payload });
export const chosenToOperateAC = (payload) => ({ type: CHOSEN_TO_OPERATE, payload });
export const addToCartAC = (payload) => ({ type: ADD_TO_CART, payload });
export const moveFromCartAC = (payload) => ({ type: MOVE_FROM_CART, payload });
export const onCheckoutAC = (payload) => ({ type: ON_CHECKOUT , payload});